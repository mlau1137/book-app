from multiprocessing import AuthenticationError
from turtle import title
from django.db import models
from django.utils.html import mark_safe

# Create your models here.
class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

class Book(models.Model):
    title = models.CharField(max_length=30, unique=True)
    author = models.ManyToManyField(Author, related_name="books")
    number_of_pages = models.SmallIntegerField(null=True, blank=True)
    isbn = models.BigIntegerField(null=True)
    if_in_print = models.BooleanField(null=True, blank=True)
    publish_date = models.SmallIntegerField(null=True)
    cover = models.URLField(null=True)

    def __str__(self):
        return self.title + " by " + self.author

    @property
    def cover_preview(self):
        if self.cover:
            return mark_safe('<img src="{}" height="150" />'.format(self.cover))
        return ""

class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

class Magazine(models.Model):
    title = models.CharField(max_length=50)
    cover = models.URLField(null=True)
    description = models.TextField(blank=True)
    release_cycle = models.TextField(blank=True)
    page_count = models.SmallIntegerField(null=True)
    mag_genre = models.ManyToManyField("Genre", related_name="mag_genre")

    def __str__(self):
        return self.title

class Genre(models.Model):
    genre_name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.genre_name

class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name='issues', on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    date_publish = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title + ": issue " + str(self.issue_number)