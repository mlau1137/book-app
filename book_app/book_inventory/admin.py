from django.contrib import admin
from .models import Book, BookReview, Magazine, Issue, Genre

class BookAdmin(admin.ModelAdmin):
    readonly_fields = ('cover_preview',)

    def cover_preview(self, obj):
        return obj.cover_preview

admin.site.register(Book, BookAdmin)
admin.site.register(Magazine)
admin.site.register(Issue)
admin.site.register(Genre)
# admin.site.register(BookReview)