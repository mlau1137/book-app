from socket import fromshare
from django import forms
from .models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "title",
            "author",
            "number_of_pages",
            "isbn",
            "if_in_print",
            "publish_date",
            "cover",
        ]

class MagForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "title",
            "cover",
            "description",
            "release_cycle",
        ]