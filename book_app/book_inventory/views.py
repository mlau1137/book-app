from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.views.generic.detail import DetailView
from .forms import BookForm, MagForm
from .models import Book, Magazine, Genre, Issue
# Create your views here.

def books_list(request):
    list_of_books = Book.objects.all()
    context = {
        "books": list_of_books,
    }
    return render(request, "books/list.html", context)

def create_book(request):
    context= {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        new_book = form.save()
        return redirect("show_book", pk = new_book.pk)
    context['form'] = form
    return render(request, "books/create_book.html", context)

def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, "books/book_detail.html", context)

def update_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk = pk)
    form = BookForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("show_book", pk=pk)
    context["form"] = form
    return render(request,"books/update_view.html", context)

def delete_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("books_list")
    return render(request, "books/delete_view.html", context)

def magazine_list_view(request):
    mag = Magazine.objects.all()
    context = {
        "magazines": mag
    }
    return render(request, "magazines/mag_list.html", context)

def create_magazine(request):
    context = {}
    form = MagForm(request.POST or None)
    if form.is_valid():
        new_mag = form.save()
        return redirect("magazine_list")
    context["form"] = form
    return render(request, "magazines/create_magazine.html", context)

def show_magazine_details(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine": magazine
    }
    return render(request, "magazines/mags_detail.html", context)

def magazine_update(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("show_magazine", pk=pk)
    context["form"] = form
    return render(request, "magazines/update_mag.html", context)

def delete_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("magazine_list")
    return render(request, "magazines/delete_mag.html", context)

def show_genre(request, pk):
    mag_genre = Genre.objects.get(pk=pk)
    context = {
        "magazine_genre": mag_genre
    }
    return render(request, "magazines/mag_genre.html", context)
