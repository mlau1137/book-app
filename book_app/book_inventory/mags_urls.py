from django.contrib import admin
from django.urls import path, include
from .views import magazine_list_view, create_magazine, show_magazine_details, magazine_update, delete_magazine, show_genre

urlpatterns = [
    path("", magazine_list_view, name="magazine_list"),
    path("create_magazine/", create_magazine, name="create_magazine"),
    path("<int:pk>/", show_magazine_details, name="show_magazine"),
    path("<int:pk>/update/", magazine_update, name="magazine_update"),
    path("<int:pk>/delete/", delete_magazine, name="delete_magazine"),
    path("genre/<int:pk>/", show_genre, name="show_genre"),
]