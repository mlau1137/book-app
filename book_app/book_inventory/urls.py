from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from .views import delete_view, show_book_details, books_list, create_book, update_view, delete_view, magazine_list_view, create_magazine, show_genre

urlpatterns = [
    path("create_book/", create_book, name="create_book"),
    path("", books_list, name="books_list"),
    path("<int:pk>/", show_book_details, name="show_book"),
    path("<int:pk>/update/", update_view, name="update_view"),
    path("<int:pk>/delete/", delete_view, name="delete_view"),
    path('accounts/login/', auth_views.LoginView.as_view(), name='login'),
]
